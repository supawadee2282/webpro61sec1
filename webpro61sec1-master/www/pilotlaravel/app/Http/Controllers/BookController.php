<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /*
    //1
    function store(Request $request){
        $book = new Books();
        $book->setAttribute("name",$request->name);
        $book->setAttribute("category",$request->category);
        $book->setAttribute("description",$request->description);
        if ($book->save()){
            return true;
        }
    }
    */
    //2
    function store(Request $request){//ถ้าใช้ตัวนี้ไปหน้าBooks
        $book = new Books();
        if (Books::created($request->all())){
            return true;
        }
    }
    function updeate(Request $request,Books $book){//อับเดตข้อมูลใน1เล่ม
        if ($book->fill($request->all())){
            return true;
        }
    }

    function index(){
        $books = Books::all();// all=select*
        return $books;
    }

    function show(Books $book){//โชว์หนังสือทีละเล่ม
        return $book;
    }

    function destroy(Books $book){//ลบข้อมูล
        if ($book->delete()){
            return true;
        }
    }
}
