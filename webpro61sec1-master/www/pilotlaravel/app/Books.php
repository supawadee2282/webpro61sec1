<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = 'books';
    protected $fillable = ['name','category','description']; //ถ้าใช้ฟังก์ชันที่2ต้องพิมตัวนี้
}
