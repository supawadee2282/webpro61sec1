function makeUser(nameValue, ageValue, addrValue, telValue, sexValue, emailValue){
	return {name : nameValue,
			age : ageValue,
			addr : addrValue,
			tel : telValue,
			sex : sexValue,
			email : emailValue};
}
function showUser(user){
	let str = "";
		for(let key in user){
			str = str+user[key]+"\n";
		}
		return str;
}
function cloneUser(user){
	let tmpUser = {};
	for (let key in user){
		tmpUser[key] = user[key];
	}
	return tmpUser;
}